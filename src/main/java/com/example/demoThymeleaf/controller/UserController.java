package com.example.demoThymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.example.demoThymeleaf.model.User;

@Controller
public class UserController {

	// private static final String appName = "ThymeleafTour";
	private final SpringTemplateEngine templateEngine;
	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	public UserController(SpringTemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	@GetMapping("/user")
	public String home(Model model, @RequestParam(value = "name", required = false, defaultValue = "Guest") String name,
			@RequestParam(value = "title", required = false, defaultValue = "Coditas") String title) {

		model.addAttribute("name", name);
		model.addAttribute("title", title);
		return "User";

	}

	List<User> theUser;

	@PostConstruct
	public void loadData() {
		User u1 = new User(1, "Rohit", "Khandelwal");
		User u2 = new User(2, "Enna", "Bella");
		User u3 = new User(3, "Gopal", "Verma");

		theUser = new ArrayList<User>();

		theUser.add(u1);
		theUser.add(u2);
		theUser.add(u3);

	}

	@GetMapping("/list")
	public String userList(Model theModel) {

		theModel.addAttribute("users", theUser);
		return "UserTable";
	}

	@GetMapping("/message")
	public String displayString() {

		Context context = new Context();
		context.setVariable("message", "Hey , how is your day");

		String processedTemplate = templateEngine.process("ABC", context);
		return processedTemplate;
	}

	@GetMapping("/SMS")
	public String getData() {

		User theUser = new User();
		theUser.setId(101);
		theUser.setFirstName("Rohit");
		theUser.setLastName("Khandelwal");
		
		NoteTemplate note = new NoteTemplate();
		note.setNoteTemplateContent(
				"<html> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"
				+ " <head><title>Cleareance of test</title>"+"<body><p>Dear [[${name}]],</p>\n" + 
						"<p>Congratulations! you have successfully passed the your test with the test Id:[[${id}]].</p> </body></html>");

		Context context = new Context();
//		context.setVariable("users", theUser);
		context.setVariable("name", "Rohit kahndelwal");
		context.setVariable("id", "101");

		String processedTemplate = templateEngine.process(note.getNoteTemplateContent(), context);

		log.info(processedTemplate);
		return processedTemplate;
	}
}