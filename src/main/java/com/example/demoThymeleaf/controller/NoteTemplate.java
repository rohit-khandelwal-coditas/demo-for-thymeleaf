package com.example.demoThymeleaf.controller;

import lombok.Data;

@Data
public class NoteTemplate {
	private String noteTemplateContent;

	public String getNoteTemplateContent() {
		return noteTemplateContent;
	}

	public void setNoteTemplateContent(String noteTemplateContent) {
		this.noteTemplateContent = noteTemplateContent;
	}
}